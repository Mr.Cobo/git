# EJERCICIO 1
Cremos el host virtual para el domino repaso.com
## Imagen  
![Logo de markdown](imagenes-Repaso/e1-1.PNG)

Creamos y configuramos el fichero configuracion repaso.conf
## Imagen  
![Logo de markdown](imagenes-Repaso/e1-2.PNG)

Lo habilitamos y reiniciamos el servidor.
## Imagen  
![Logo de markdown](imagenes-Repaso/e1-3.PNG)

Y comprobamos que funciona, como en /etc/apache2/repaso no se encuentra ningun fichero con el nombre index.html nos indexa todos los contenidos de ese directorio, en este caso no hay ninguno.
## Imagen  
![Logo de markdown](imagenes-Repaso/e1-4.PNG)

Y subimos cambios al repositorio
## Imagen  
![Logo de markdown](imagenes-Repaso/e1-5.PNG)

# EJERCICIO 2
Creamos el fichero de autenticacion usuariosRepasoBasic en el directorio /etc/apache2 y habilitamos el usuario admin

## Imagen  
![Logo de markdown](imagenes-Repaso/e2-1.PNG)

Comprobamos que se ha creado la carpeta "yp" correctamente
## Imagen  
![Logo de markdown](imagenes-Repaso/e2-2.PNG)

Configuramos el  acceso del directorio /var/www/repaso/yo

## Imagen  
![Logo de markdown](imagenes-Repaso/e2-4.PNG)

Y subimos cambios
## Imagen  
![Logo de markdown](imagenes-Repaso/e2-3.PNG)

# EJERCICIO 3
Limitando el acceso para que solo se pueda acceder desde las nueve de la mañana hasta las dos de la tarde
## Imagen  
![Logo de markdown](imagenes-Repaso/e3-1.PNG)

Comprobando que funciona
## Imagen  
![Logo de markdown](imagenes-Repaso/e3-2.PNG)

Limitando el acceso para que solo se pueda acceder desde las nueve de la mañana hasta las dos de la tarde
## Imagen  
![Logo de markdown](imagenes-Repaso/e3-3.PNG)

Comprobando que funciona, hora de prueba, las 20:00
## Imagen  
![Logo de markdown](imagenes-Repaso/e3-4.PNG)

Entrando al directorio restringuido
## Imagen  
![Logo de markdown](imagenes-Repaso/e3-5.PNG)


## Imagen  
![Logo de markdown](imagenes-Repaso/e3-6.PNG)


## Imagen  
![Logo de markdown](imagenes-Repaso/e3-7.PNG)
