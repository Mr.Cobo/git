# Ejercicio 1
Para crear una nueva rama
```bash
git branch nombreRama
```
Para posicionarse en esa rama
```bash
git checkout nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e1.PNG)
# Ejercicio 2
Creamos un fichero con touch, lo editamos y hacemos un commit
```bash
touch daw.md
git commit -m "Añadiendo fichero en la rama"
git add .
git push origin nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e2.PNG)
# Ejercicio 3
Nos posicionamos en la rama master y hacemos un merge
```bash
git checkout master
git merge nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e3.PNG)
# Ejercicio 4
Modificamos el fichero daw.md
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-2.PNG)
Subimos los cambios al repositorio estando en la rama master
```bash
git add .
git commit -m "Añadida tabla 1"
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-1.PNG)

Ahora escribimos en daw.md los modulos de segundo en la rama creada antes
```bash
git checkout nombreRama
git add .
git commit -m "Añadida tabla 2"
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-3.PNG)

Nos posicionamos en la otra rama y modificamos el fichero daw.md
```bash
git checkout nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-3.PNG)
Ahora escribimos en daw.md los modulos de segundo en la rama creada antes
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-4.PNG)
Ahora fusionamos ramas 
```bash
git add .
git commit -m "añadida tabla 2"
# Cambiamos a la rama master
git checkout master
git merge nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-44.PNG)

# Ejercicio 5  
Ahora nos toca solucionar los errores.  
Como puede verse en la imagen hay varias lineas que debemos borrar a mano y luego subirlas al repositorio
## Imagen  
![Logo de markdown](imagenes-Ej3/e5-1.PNG)
Quedando algo como:

## Imagen  
![Logo de markdown](imagenes-Ej3/e5-2.PNG)

Y subimos los cambios al repositorio:
```bash
git add .
git commit -m "solucionando cambios de daw.md"
git push origin master
```

## Imagen  
![Logo de markdown](imagenes-Ej3/e5-3.PNG)

# Ejercicio 6
Creamos un tag y borramos la rama
```bash
git tag 0.2
git branch -d nombreRama
```

## Imagen  
![Logo de markdown](imagenes-Ej3/e6-1.PNG)