# HOJA03_MARKDOWN_03

# Ejercicio 1
Para crear una nueva rama
```bash
git branch nombreRama
```
Para posicionarse en esa rama
```bash
git checkout nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e1.PNG)
# Ejercicio 2
Creamos un fichero con touch, lo editamos y hacemos un commit
```bash
touch daw.md
git commit -m "Añadiendo fichero en la rama"
git add .
git push origin nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e2.PNG)
# Ejercicio 3
Nos posicionamos en la rama master y hacemos un merge
```bash
git checkout master
git merge nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e3.PNG)
# Ejercicio 4
Añadimos en el fichero daw.md una tabla con los modulos del primer curso.
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-2.PNG)

Añadimos cambios y hacemos un commit
```bash
git add .
git commit -m "Añadida tabla 1"
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-33.PNG)

Ahora escribimos en daw.md los modulos de segundo en la rama creada antes
```bash
git checkout nombreRama
git add .
git commit -m "Añadida tabla 2"
```

## Imagen  
![Logo de markdown](imagenes-Ej3/e4-3.PNG)

Ahora escribimos en daw.md los modulos de segundo en la rama creada antes
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-4.PNG)

Fusionamos ramas.
```bash
git add .
git commit -m "añadida tabla 2"
# Cambiamos a la rama master
git checkout master
git merge nombreRama
```
## Imagen  
![Logo de markdown](imagenes-Ej3/e4-44.PNG)

# Ejercicio 5  
Ahora nos toca solucionar los errores.  
Como puede verse en la imagen hay varias lineas que debemos borrar a mano y luego subirlas al repositorio
## Imagen  
![Logo de markdown](imagenes-Ej3/e5-1.PNG)
Quedando algo como:

## Imagen  
![Logo de markdown](imagenes-Ej3/e5-2.PNG)

Y subimos los cambios al repositorio:
```bash
git add .
git commit -m "solucionando cambios de daw.md"
git push origin master
```

## Imagen  
![Logo de markdown](imagenes-Ej3/e5-3.PNG)

# Ejercicio 6
Creamos un tag y borramos la rama
```bash
git tag 0.2
git branch -d nombreRama
```

## Imagen  
![Logo de markdown](imagenes-Ej3/e6-1.PNG)
  
  
  
  



  
  
# HOJA03_MARKDOWN_02

# Ejecicio 1

# Ejecicio 2
**Para clonar un repositori usamos el comando git clone:**  
```bash
git clone 'urlRepositorio'
```
# Ejecicio 3

## Imagen  
![Logo de markdown](imagenes/e3.PNG)

![Logo de markdown](imagenes/e3-1.PNG)

# Ejecicio 4  
Los comandos mas utilizados hasta ahora:  
```bash
cd repositorio
git init 
git add md master
git add .
git commit -m "Comentario"
git push md master
```

# Ejecicio 5
**Para que determinados ficheros no se suban al repositorio debemos crear el fichero .gitignore
y escribir los nombre de los ficheros o directorios que no queremos que se suban**  
## Imagen  
![Logo de markdown](imagenes/e4.PNG)
# Ejecicio 6
**Creamos el fichero privado.txt y tambien la carpeta privada**  
```bash
touch privado.txt
mkdir privada
```
![Logo de markdown](imagenes/e6.PNG)

# Ejecicio 7
## Mostrando listado de los modulos 
1. DWES
2. DWEC
3. EIE
4. DIW
5. DAW  

# Ejecicio 8
**Para realizar el tag se debe utilizar el comando git tag.Ejemplo:**   
```bash
git tag v0.1
```

# Ejecicio 9
**Y despues hacemos un git push** 
```bash
git push origin master
``` 
# Ejercicio 10
Enlace | Descripcion
-------|-------------
[Enlace a la documentacion de apache](http://www.apache.org) | Apache




